//
//  Experience.swift
//  Labb 2 inlämning
//
//  Created by Aziz Ali on 2018-12-10.
//  Copyright © 2018 Aziz Ali. All rights reserved.
//

import Foundation

class Experience {
    var title : String
    var description : String
    var duration :String
    var image : String
    init(title:String, description:String, duration:String, image:String) {
        self.title = title
        self.description = description
        self.duration = duration
        self.image = image
    }
}
